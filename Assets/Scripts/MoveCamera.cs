﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class MoveCamera : MonoBehaviour
{
    [Header("Ray Param")]
    [Range(0,10)]
    public float rangeRay = 1;
    public LayerMask playerMask;

    CinemachineFramingTransposer camBrain;

    [Header("Modif Right Ray")]
    [Range(0, 1)]
    public float ScreenXRight = .5f;
    [Range(0, 1)]
    public float ScreenYRight = .5f;

    [Header("Modif Left Ray")]
    [Range(0, 1)]
    public float ScreenXLeft = .5f;
    [Range(0, 1)]
    public float ScreenYLeft = .5f;

    // Start is called before the first frame update
    void Start()
    {
        camBrain = FindObjectOfType< CinemachineFramingTransposer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if ((this.transform.position.x - collision.transform.position.x) > 0)
            {
                camBrain.m_ScreenX = ScreenXLeft;
                camBrain.m_ScreenY = ScreenYLeft;
            }
            else if ((this.transform.position.x - collision.transform.position.x) < 0)
            {
                camBrain.m_ScreenX = ScreenXRight;
                camBrain.m_ScreenY = ScreenYRight;
            }
        }
    }
}
