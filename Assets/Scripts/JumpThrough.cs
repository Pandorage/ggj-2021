﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpThrough : MonoBehaviour
{
    public Collider2D edge;
    public PlatformEffector2D platform;
    public Collider2D box;

    private void Update()
    {
        if (MechaManager.Instance.Check(MechaManager.Mecha.JumpThrough))
        {
            edge.enabled = true;
            platform.enabled = true;
            box.enabled = false;
        }
        else
        {
            edge.enabled = false;
            platform.enabled = false;
            box.enabled = true;
        }
    }
}
