﻿using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    private Transform _parent;
    private Vector2 _startPos;
    public Vector2 movement;
    public float speed = 1;

    void Start()
    {
        _parent = transform.parent;
        _startPos = _parent.position;
    }
    
    void Update()
    {
        _parent.position = _startPos + movement * Mathf.Sin(Time.time * speed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.transform.parent = _parent;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.transform.parent = null;
        }
    }
}
