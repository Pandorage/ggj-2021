﻿using System.Collections;
using UnityEngine;

public class CrumblingPlatform : MonoBehaviour
{
    public Animator anim;
    public Collider2D coll;
    public float crumbleTime;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Player"))
        {
            StartCoroutine(Crumble());
        }
    }

    IEnumerator Crumble()
    {
        yield return new WaitForSeconds(crumbleTime-0.1f);
        anim.SetTrigger("crumble");
        yield return new WaitForSeconds(0.1f);
        coll.enabled = false;
    }

    public void Reset()
    {
        StopAllCoroutines();
        anim.SetTrigger("unCrumble");
        coll.enabled = true;
    }
}
