﻿using UnityEngine;

public class OpeningDoor : MonoBehaviour
{
    public Collider2D doorCollider;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponent<Animator>().SetTrigger("Open");
        }
    }

    public void Open()
    {
        doorCollider.enabled = false;
    }
}
