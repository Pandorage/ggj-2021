﻿using UnityEngine;

namespace Player
{
    public class BetterJumping : MonoBehaviour
    {
        private Rigidbody2D _rb;
        public float fallMultiplier = 2.5f;
        public float lowJumpMultiplier = 2f;

        void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            if(_rb.velocity.y * Mathf.Sign(_rb.gravityScale) < 0)
            {
                _rb.velocity += (Vector2)transform.up.normalized * (Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime);
            }else if(_rb.velocity.y * Mathf.Sign(_rb.gravityScale) > 0 && !Input.GetButton("Jump"))
            {
                _rb.velocity += (Vector2)transform.up.normalized * (Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime);
            }
        }
    }
}