﻿using DG.Tweening;
using UnityEngine;

public class Grapple : MonoBehaviour
{
    public LineRenderer line;
    public DistanceJoint2D joint;
    public Transform player;
    public Transform testPosGrapple;

    private void Update()
    {
        line.SetPosition(0,transform.position);
        line.SetPosition(1,player.position);
    }

    //TODO detect and grab the nearest point in range
    public void Grab()
    {
        transform.parent = null;
        transform.DOMove(testPosGrapple.position, 0.1f).OnComplete(() =>
        {
            joint.connectedBody = player.GetComponent<Rigidbody2D>();
        });
    }

    public void UnGrab()
    {
        joint.connectedBody = null;
        transform.parent = player;
        transform.DOLocalMove(Vector3.zero, 0.1f).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
}