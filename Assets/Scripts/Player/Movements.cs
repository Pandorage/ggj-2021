﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Player
{
    public class Movements : MonoBehaviour
    {
        public float speed = 5;
        public float jumpForce = 12;
        public float dashSpeed = 20;
        public float dashDuration = 0.1f;
        public float gravityCooldown;
        public Grapple grapple;
        public float swingForce;
        public Animator anim;
        public Transform ladder;
        public bool pauseMove;

        private Rigidbody2D _rb;
        private Collisions _coll;
        private BetterJumping _betterJump;
        private bool _hasDoubleJumped;
        private bool _hasDashed;
        private bool _isDashing;
        private float _gScale;
        private int _gModifier = 1;
        private float _nextGravity;
        private bool _isGrappling;
        private List<Collider2D> _jumpThrough = new List<Collider2D>();
        private bool _isClimbing;
        private float _minTimeClimb;
        
        void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
            _gScale = _rb.gravityScale;
            _coll = GetComponent<Collisions>();
            _betterJump = GetComponent<BetterJumping>();
        }
        
        void Update()
        {
            if(pauseMove)
                return;
            anim.SetBool("isGrounded", _coll.onGround);
            //Reset double jump and dash on ground
            if ((_hasDoubleJumped || _hasDashed) && _coll.onGround)
            {
                _hasDoubleJumped = false;
                _hasDashed = false;
            }
            //Get movement axis
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            Vector2 dir = new Vector2(x, y);

            anim.SetFloat("speed", Mathf.Abs(x));
            anim.SetFloat("ySpeed", Mathf.Abs(y));
            anim.SetFloat("yVelocity", _rb.velocity.y);

            if (ladder != null && !_isDashing && !_isGrappling && !_isClimbing)
            {
                StartClimb();
            }

            if (_isClimbing)
            {
                if (ladder == null ||
                    (Mathf.Abs(dir.x) == 1 && ladder.CompareTag("Ladder") ||
                     dir.y < 0 && !ladder.CompareTag("Ladder")) && Time.time >= _minTimeClimb)
                {
                    StopClimb();
                }
                else
                {
                    Climb(dir);
                }
            }

            //Change player y rotation to face movement direction
            if (x > 0)
            {
                var angle = transform.eulerAngles;
                angle.y = _gModifier > 0 ? 0 : 180;
                transform.eulerAngles = angle;
            }
            else if (x < 0)
            {
                var angle = transform.eulerAngles;
                angle.y = _gModifier > 0 ? 180 : 0;
                transform.eulerAngles = angle;
            }

            //On down input, fall through the platform if possible
            if (y < 0 && _jumpThrough.Count > 0)
            {
                foreach (var coll in _jumpThrough)
                {
                    StartCoroutine(FallThrough(coll));
                }
            }

            //Move the player
            if (!_isGrappling && !_isDashing && !_isClimbing)
            {
                Walk(dir.x);
            }

            //On jump input
            if (Input.GetButtonDown("Jump") && MechaManager.Instance.Check(MechaManager.Mecha.Jump) &&
                (_coll.onGround || !_hasDoubleJumped && MechaManager.Instance.Check(MechaManager.Mecha.DoubleJump)) && !_isGrappling)
            {
                //If not on ground, consume double jump
                if (!_coll.onGround && !_isClimbing)
                {
                    _hasDoubleJumped = true;
                }

                if (_isClimbing)
                {
                    StopClimb();
                }
                //Jump
                Jump();
            }

            //TODO Define input
            //On dash input, dash in player facing direction
            if (Input.GetButtonDown("Fire1") && MechaManager.Instance.Check(MechaManager.Mecha.Dash) && !_hasDashed &&
                !_isDashing && !_isGrappling && !_isClimbing)
            {
                StartCoroutine(Dash());
            }
            
            //On gravity input, invert player gravity
            if (Input.GetButtonDown("Fire3") && MechaManager.Instance.Check(MechaManager.Mecha.Gravity) &&
                Time.time >= _nextGravity && !_isGrappling && !_isClimbing)
            {
                _nextGravity = Time.time + gravityCooldown;
                Gravity();
            }

            //TODO grapple cooldown & disable betterjump
            //On grapple input down, lauch grapple hook
            if (Input.GetButtonDown("Fire2") && MechaManager.Instance.Check(MechaManager.Mecha.Grapple) && !_isClimbing)
            {
                _isGrappling = true;
                _betterJump.enabled = false;
                grapple.gameObject.SetActive(true);
                grapple.Grab();
            }

            //On grapple input up, release grapple hook
            if (Input.GetButtonUp("Fire2") && MechaManager.Instance.Check(MechaManager.Mecha.Grapple))
            {
                grapple.UnGrab();
                _betterJump.enabled = true;
                _isGrappling = false;
            }
        }

        private void FixedUpdate()
        {
            if (_isGrappling)
            {
                HandleGrappling(Input.GetAxis("Horizontal"));
            }
        }

        private void Walk(float xInput)
        {
            _rb.velocity = new Vector2(xInput * speed, _rb.velocity.y);
        }

        private void Jump()
        {
            anim.SetTrigger("jump");
            _rb.velocity = new Vector2(_rb.velocity.x, 0);
            _rb.velocity += (Vector2)transform.up.normalized * jumpForce;
        }

        IEnumerator Dash()
        {
            anim.SetBool("isDashing", true);
            _hasDashed = true;
            _isDashing = true;
            _rb.gravityScale = 0;
            _betterJump.enabled = false;
            _rb.velocity = transform.right.normalized * dashSpeed;
            yield return new WaitForSeconds(dashDuration);
            _rb.gravityScale = _gScale * _gModifier;
            _betterJump.enabled = true;
            _isDashing = false;
            anim.SetBool("isDashing", false);
        }

        private void Gravity()
        {
            _gModifier *= -1;
            var angle = transform.eulerAngles;
            angle.y += 180;
            transform.eulerAngles = angle;
            transform.Rotate(0,0,180);
            _rb.gravityScale *= -1;
        }

        private void HandleGrappling(float horizontalInput)
        {
            //TODO Handle grapple with gravity change
            //Get a normalized direction vector from the player to the hook point
            Vector2 playerToHookDirection = (grapple.transform.position - transform.position).normalized;

            //Inverse the direction to get a perpendicular direction
            Vector2 perpendicularDirection = Vector2.zero;
            if (horizontalInput < 0)
            {
                perpendicularDirection = new Vector2(-playerToHookDirection.y, playerToHookDirection.x);
                var leftPerpPos = (Vector2)transform.position - perpendicularDirection * -2f;
                Debug.DrawLine(transform.position, leftPerpPos, Color.green, 0f);
            }
            else if(horizontalInput > 0)
            {
                perpendicularDirection = new Vector2(playerToHookDirection.y, -playerToHookDirection.x);
                var rightPerpPos = (Vector2)transform.position + perpendicularDirection * 2f;
                Debug.DrawLine(transform.position, rightPerpPos, Color.green, 0f);
            }

            var force = perpendicularDirection * swingForce;
            _rb.AddForce(force, ForceMode2D.Force);
        }

        IEnumerator FallThrough(Collider2D coll)
        {
            coll.isTrigger = true;
            yield return new WaitForSeconds(0.2f);
            coll.isTrigger = false;
        }

        private void StartClimb()
        {
            if (ladder.CompareTag("Ladder"))    
            {
                transform.DOMove(new Vector2(ladder.position.x, transform.position.y), 0.1f);
                anim.SetBool("isClimbing", true);
            }
            else
            {
                transform.DOMove(new Vector2(transform.position.x, ladder.position.y+0.4f), 0.1f);
                anim.SetBool("isHanging", true);
            }
            
            _minTimeClimb = Time.time + 0.1f;
            _isClimbing = true;
            _rb.gravityScale = 0;
            _betterJump.enabled = false;
            _hasDoubleJumped = false;
            _hasDashed = false;
        }
        private void Climb(Vector2 dir)
        {
            if (ladder.CompareTag("Ladder"))
            {
                _rb.velocity = new Vector2(0, dir.y * speed/2);
            }
            else
            {
                _rb.velocity = new Vector2(dir.x * speed/2, 0);
            }
        }

        private void StopClimb()
        {
            anim.SetBool("isClimbing", false);
            anim.SetBool("isHanging", false);
            ladder = null;
            _isClimbing = false;
            _rb.gravityScale = _gScale * _gModifier;
            _betterJump.enabled = true;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("JumpThrough"))
            {
                if (!_jumpThrough.Contains(other.collider))
                {
                    _jumpThrough.Add(other.collider);
                }
            }
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("JumpThrough"))
            {
                _jumpThrough.Remove(other.collider);
            }
        }
    }
}