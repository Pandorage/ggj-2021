﻿using UnityEngine;

namespace Player
{
    public class Collisions : MonoBehaviour
    {
        public LayerMask groundLayer;
        public bool onGround;
        public float collisionRadius = 0.25f;
        public Transform groundChecker;
        
        void Update()
        {
            onGround = Physics2D.OverlapCircle(groundChecker.position, collisionRadius, groundLayer);
        }
        
        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            Gizmos.DrawWireSphere(groundChecker.position, collisionRadius);
        }
    }
}