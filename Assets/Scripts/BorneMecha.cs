﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Player;
using UnityEngine;
using Random = UnityEngine.Random;

public class BorneMecha : MonoBehaviour
{
    public List<MechaManager.Mecha> added;
    public List<MechaManager.Mecha> removed;
    public SpriteRenderer icon;
    public SpriteRenderer iconBase;
    public Sprite green;
    public Sprite red;
    
    private bool _used;
    private List<MechaManager.Mecha> _mechalist;
    private int _index;

    private void Start()
    {
        _mechalist = new List<MechaManager.Mecha>(added);
        _mechalist.AddRange(removed);
        StartCoroutine(ChangeDisplay());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !_used)
        {
            StartCoroutine(ChangeMecha(other.GetComponent<Movements>()));
        }
    }

    IEnumerator ChangeMecha(Movements player)
    {
        _used = true;
        player.pauseMove = true;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.transform.DOMove(new Vector2(transform.position.x, player.transform.position.y), 0.2f);
        player.anim.SetTrigger("Victory");
        player.anim.SetFloat("speed", 0);
        foreach (var mecha in added)
        {
            MechaManager.Instance.Add(mecha);
        }
        foreach (var mecha in removed)
        {
            MechaManager.Instance.Remove(mecha);
        }
        yield return new WaitForSeconds(1);
        player.pauseMove = false;
    }

    IEnumerator ChangeDisplay()
    {
        var mecha = _mechalist[_index];
        icon.sprite = MechaManager.Instance.GetMechaSprite(mecha);
        if (added.Contains(mecha))
        {
            iconBase.sprite = green;
        }
        else
        {
            iconBase.sprite = red;
        }

        _index++;
        if (_index == _mechalist.Count)
        {
            _index = 0;
        }
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ChangeDisplay());
    }
}
