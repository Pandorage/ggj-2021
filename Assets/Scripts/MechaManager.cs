﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;

public class MechaManager : MonoBehaviour
{
    public static MechaManager Instance;
    [System.Flags]
    public enum Mecha
    {
        Jump = 1<<1,
        DoubleJump = 1<<2,
        Dash = 1<<3,
        Gravity = 1<<4,
        Grapple = 1<<5,
        JumpThrough = 1<<6
    }
    public Mecha mecha;
    public SpriteResolver head;
    public SpriteResolver leftLeg;
    public SpriteResolver rightLeg;
    public GameObject antenna;
    public List<MechaIcon> icons;

    /*
     * Define multiple states: var t = Mecha.DoubleJump | Mecha.Dash | Mecha.Gravity
     * Remove all states: var t = Mecha.None
     * Remove one state: t &= ~Mecha.DoubleJump
     * Add a state : t |= Mecha.DoubleJump
     * Check a state : if(t & Mecha.DoubleJump > 0){ }
     * Check multiple states : if(b & (Mecha.DoubleJump | Mecha.Dash) > 0){ }
     */
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    //Check if a state is enabled
    public bool Check(Mecha toCheck)
    {
        return (mecha & toCheck) > 0;
    }

    //Add a state
    public void Add(Mecha toAdd)
    {
        mecha |= toAdd;
        switch (toAdd)
        {
            case Mecha.Dash:
                head.SetCategoryAndLabel("Head","Head_Dash");
                break;
            case Mecha.DoubleJump:
                leftLeg.SetCategoryAndLabel("Left leg","LeftLeg_Long");
                rightLeg.SetCategoryAndLabel("Right leg","RightLeg_Long");
                break;
            case Mecha.Gravity:
                antenna.SetActive(true);
                break;
        }
    }

    //Remove a state
    public void Remove(Mecha toRemove)
    {
        mecha &= ~toRemove;
        switch (toRemove)
        {
            case Mecha.Dash:
                head.SetCategoryAndLabel("Head","Head");
                break;
            case Mecha.DoubleJump:
                leftLeg.SetCategoryAndLabel("Left leg","LeftLeg");
                rightLeg.SetCategoryAndLabel("Right leg","RightLeg");
                break;
            case Mecha.Gravity:
                antenna.SetActive(false);
                break;
        }
    }

    public Sprite GetMechaSprite(Mecha toGet)
    {
        foreach (var mechaIcon in icons)
        {
            if ((mechaIcon.mecha & toGet) > 0)
            {
                return mechaIcon.icon;
            }
        }
        return null;
    }

    [Serializable]
    public class MechaIcon
    {
        public Mecha mecha;
        public Sprite icon;
    }
}
