﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class SetPlayerPos
{
    public static Vector3 pos  = new Vector3(-7.53f, .7f,0f);

    [MenuItem("Window/LD/ResetPos %g")]
    public static void ResetPos()
    {
        Debug.Log("position reset");
        GameObject.FindGameObjectWithTag("Player").transform.position = pos;
    }

    [MenuItem("Window/LD/SetPos %f")]
    public static void SetPos()
    {
        Debug.Log("position set");
        GameObject.FindGameObjectWithTag("Player").transform.position = Selection.activeGameObject.transform.position;
    }
}
